package com.qaagility.controller;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    @Test
    public void testDesc() {
        Calculator calculator = new Calculator();
        assertEquals(calculator.add(), 9);
    }
}
