package com.qaagility.controller;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DividorTest {

    @Test
    public void testDivide() {
        Dividor dividor = new Dividor();
        assertEquals(dividor.divide(8, 4), 2);
    }

    @Test
    public void testDivide_byZero() {
        Dividor dividor = new Dividor();
        assertEquals(dividor.divide(8, 0), Integer.MAX_VALUE);
    }
}
